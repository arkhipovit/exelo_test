<?php

require(__DIR__ . '/app/bootstrap.php');
require(APP_PATH_ROOT . '/vendor/autoload.php');
require(APP_PATH_ROOT . '/app/bootstrap_yii.php');

$config = require(APP_PATH_ROOT . '/app/config/cli/main.php');
$application = (new yii\console\Application($config));

$exitCode = $application->run();
exit($exitCode);
