<?php

ini_set('default_charset', 'UTF-8');
date_default_timezone_set('UTC');
set_include_path('.');

error_reporting(-1);
ini_set('log_errors', 0);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);

define('APP_PATH_ROOT', dirname(__DIR__));

define('APP_ENV_PRODUCTION', 'prod');
define('APP_ENV_DEVELOPMENT', 'dev');
define('APP_ENV_TEST', 'test');

if (file_exists(APP_PATH_ROOT . '/app/environment.php')) {
    require(APP_PATH_ROOT . '/app/environment.php');
}
if (!defined('APP_ENVIRONMENT')) {
    define('APP_ENVIRONMENT', APP_ENV_PRODUCTION);
}
if (!defined('APP_DEBUG')) {
    define('APP_DEBUG', false);
}
