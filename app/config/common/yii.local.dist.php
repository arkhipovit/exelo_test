<?php

return [
    'components' => [
        'db' => [
            'enableSchemaCache' => false
        ]
    ]
];
