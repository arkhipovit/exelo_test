<?php

namespace app\controllers;

use app\models\Proxy as arProxy;
use Yii;

class DefaultController extends \yii\console\Controller
{
    function actionRandom($num = 20)
    {
        if (!is_numeric($num)) {
            throw new \InvalidArgumentException("Wrong param type.");
        }

        $query = arProxy::find()
            ->orderBy('RAND()')
            ->limit($num);

        /** @var arProxy $iProxy */
        foreach ($query->each() as $iProxy) {
            $ip = long2ip($iProxy->ip);
            $port = $iProxy->port;

            $this->stdout("{$ip}:{$port}\n");
        }
    }
}
