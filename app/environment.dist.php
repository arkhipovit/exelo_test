<?php

if (!defined('APP_ENVIRONMENT')) {
    define('APP_ENVIRONMENT', APP_ENV_DEVELOPMENT);
}
if (!defined('APP_DEBUG')) {
    define('APP_DEBUG', true);
}
