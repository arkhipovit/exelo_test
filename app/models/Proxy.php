<?php

namespace app\models;

class Proxy extends \yii\db\ActiveRecord
{
    static function tableName()
    {
        return 'proxy';
    }

    function beforeSave($insert)
    {
        if ($insert) {
            $this->timeCreated = time();
        }

        return parent::beforeSave($insert);
    }
}
